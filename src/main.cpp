#include <iostream>
#include <future>
#include <exception>
#include "xml_to_json_converter.h"

int main(int argc, char **argv)
{
    try {
        if(argc != 2) /// Должен быть только один аргумент, кроме имени программы
        {
            std::cout << "Bad programm args count" << std::endl;
            return EXIT_FAILURE;
        }
        XmlToJsonConverter file_converter(argv[1]);

        /// Сначала в отдельном потоке запускается функция для получения нужных параметров из входного файла
        auto read_xml_handler = std::async(&XmlToJsonConverter::readXmlInputFile, &file_converter);
        auto read_xml_result = read_xml_handler.get();
        if(read_xml_result == false)
        {
            std::cout << "Parsing input xml file error" << std::endl;
            return EXIT_FAILURE;
        }

        /// В случае успешного завершения запускается функция создания файла в формате json (в отдельном потоке)
        auto write_json_handler = std::async(&XmlToJsonConverter::writeJsonOutputFile, &file_converter);
        auto write_json_result = write_json_handler.get();
        if(write_json_result == false)
        {
            std::cout << "Write output json file error" << std::endl;
            return EXIT_FAILURE;
        }
    } catch (const char *msg) {
        std::cout << "Exception handling [const char*]: " << msg << std::endl;
        return EXIT_FAILURE;
    } catch (const std::exception& e) {
        std::cout << "Exception handling [const std::exception]: " << e.what() << std::endl;
        return EXIT_FAILURE;
    } catch (...) {
        std::cout << "Exception handling [other]" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "File succesfully converted!" << std::endl;

    return EXIT_SUCCESS;
}
