#include <iostream>
#include <boost/lexical_cast.hpp>
#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include "xml_to_json_converter.h"

XmlToJsonConverter::XmlToJsonConverter(std::string input_filename)
    : input_filename(input_filename)
{
    if(input_filename.size() == 0)
        throw "Zero input file name length";
}

bool XmlToJsonConverter::readXmlInputFile()
{
    std::unique_ptr <rapidxml::xml_document<>> xml_doc(new rapidxml::xml_document<>);

    std::ifstream input_file;
    input_file.open(input_filename);
    if(input_file.fail() == true)
    {
        std::cout << "File does not exist" << std::endl;
        return false;
    }
    /// Вычитываем содержимое файла в буфер
    std::vector <char> buffer((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
    if(buffer.size() == 0)
    {
        std::cout << "File is empty" << std::endl;
        return false;
    }
    buffer.push_back('\0');

    try {
        xml_doc->parse<0>(&buffer[0]);
    } catch (const rapidxml::parse_error &e) {
        std::cout << e.what() << std::endl;
        throw;
    }

    rapidxml::xml_node<> *root_node = xml_doc->first_node("data");
    if(root_node != 0) {
        unsigned int nodes_count = 0;
        /// Проверка на случай неккоректной глубины xml файла
        for(rapidxml::xml_node<> *child = root_node->first_node(); child != 0; child = child->next_sibling())
        {
            nodes_count++;
            if(nodes_count > 2)
            {
                std::cout << "Xml nodes count greater than 2" << std::endl;
                return false;
            }
        }
        /// Ищем заданные узлы
        auto child_node = root_node->first_node("msisdn");
        if(child_node == 0)
        {
            std::cout << "Xml node \"msisdn\" not found" << std::endl;
            return false;
        }
        try {
            msisdn = boost::lexical_cast <long long> (child_node->value());
        } catch (const boost::bad_lexical_cast &e) {
            std::cout << "Convert value exception: " << e.what() << std::endl;
            throw;
        }

        child_node = root_node->first_node("balance");
        if(child_node == 0)
        {
            std::cout << "Xml node \"balance\" not found" << std::endl;
            return false;
        }
        try {
            balance = boost::lexical_cast <double> (child_node->value());
        } catch (const boost::bad_lexical_cast &e) {
            std::cout << "Convert value exception: " << e.what() << std::endl;
            throw;
        }
    } else {
        std::cout << "Xml root node \"data\" not found" << std::endl;
        return false;
    }

    return true;
}

bool XmlToJsonConverter::writeJsonOutputFile()
{
    std::unique_ptr <rapidjson::Document> js_doc(new rapidjson::Document);
    js_doc->SetObject();
    if(js_doc->IsObject() == false)
    {
        std::cout << "Json create main object failed" << std::endl;
        return false;
    }
    rapidjson::Document::AllocatorType& allocator = js_doc->GetAllocator();

    rapidjson::Value msisdn_value, balance_value;
    msisdn_value.SetObject();
    if(msisdn_value.IsObject() == false)
    {
        std::cout << "Json create param 1 object failed" << std::endl;
        return false;
    }
    auto& ret_value_msisdn = js_doc->AddMember(rapidjson::StringRef("msisdn"), msisdn_value.SetInt64(msisdn), allocator);
    if(ret_value_msisdn.IsNull() == true)
    {
        std::cout << "Json write param 1 failed" << std::endl;
        return false;
    }

    balance_value.SetObject();
    if(balance_value.IsObject() == false)
    {
        std::cout << "Json create param 2 object failed" << std::endl;
        return false;
    }
    auto& ret_value_balance = js_doc->AddMember(rapidjson::StringRef("balance"), balance_value.SetDouble(balance), allocator);
    if(ret_value_balance.IsNull() == true)
    {
        std::cout << "Json write param 2 failed" << std::endl;
        return false;
    }

    rapidjson::StringBuffer buffer;
    try {
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        if(js_doc->Accept(writer) == false)
        {
            std::cout << "Json prepare write buffer failed" << std::endl;
            return false;
        }

        std::string output_json_filename = input_filename;
        if( output_json_filename.find(".xml") != std::string::npos ) {
            output_json_filename.erase(output_json_filename.find(".xml"));
            output_json_filename += ".json";
        }

        std::ofstream output_file(output_json_filename);
        output_file << buffer.GetString();
        output_file.close();
    } catch (...) {
        throw;
    }

    return true;
}

long long XmlToJsonConverter::getMsisdnParam() const
{
    return msisdn;
}

double XmlToJsonConverter::getBalanceParam() const
{
    return balance;
}

std::string XmlToJsonConverter::getInputFilename() const
{
    return input_filename;
}
