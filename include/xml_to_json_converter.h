#ifndef XML_TO_JSON_CONVERTER
#define XML_TO_JSON_CONVERTER

#include <string>

/**
 * @brief The XmlToJsonConverter struct Класс, отвечающий за реализацию процесса
 * преобразования файла из формата xml в формат json
 */
struct XmlToJsonConverter
{
    /**
     * @brief XmlToJsonConverter Конструктор класса
     * @param input_filename Имя входного файла в формате xml
     */
    explicit XmlToJsonConverter(std::string input_filename);
    /**
     * @brief readXmlInputFile Функция обработки xml файла и получения необходимых параметров
     * @return True/False Успешность процесса
     */
    bool readXmlInputFile();
    /**
     * @brief writeJsonOutputFile Функция создания файла в формате json для записи полученных параметров
     * @return True/False Успешность процесса
     */
    bool writeJsonOutputFile();
    /**
     * @brief getMsisdnParam Получение текущего значения параметра msisdn для возможности
     * проверки корректности обработки xml файла
     * @return Текущее значение параметра msisdn
     */
    long long getMsisdnParam() const;
    /**
     * @brief getBalanceParam Получение текущего значения параметра balance для возможности
     * проверки корректности обработки xml файла
     * @return Текущее значение параметра balance
     */
    double getBalanceParam() const;
    /**
     * @brief getInputFilename Получение текущего значения имени файла для возможности проверки
     * @return Текущее значение имени файла
     */
    std::string getInputFilename() const;
private:
    std::string input_filename = "default_output.json"; /// Имя входного файла в формате xml
    long long msisdn = 0;                       /// Параметр из секции msisdn входного xml файла
    double balance = 0.0;                       /// Параметр из секции balance входного xml файла
};

#endif // XML_TO_JSON_CONVERTER
