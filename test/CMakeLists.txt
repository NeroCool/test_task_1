cmake_minimum_required(VERSION 3.10)

set(PROJECT xml_to_json_tests)
project(${PROJECT})

find_package(Boost COMPONENTS unit_test_framework filesystem REQUIRED)

set(SOURCES xml_to_json_converter_test.cpp ../src/xml_to_json_converter.cpp)
set(HEADERS ../include/xml_to_json_converter.h)

add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})

target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})

add_test(NAME Tests COMMAND ${PROJECT_NAME})

