#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE XmlToJsonConverterTests
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include "../include/xml_to_json_converter.h"

BOOST_AUTO_TEST_SUITE(XML_TO_JSON_CONVERTER_TESTS)

BOOST_AUTO_TEST_CASE(InitObject)
{
    XmlToJsonConverter object(std::string("no_file.xml"));
    BOOST_CHECK(object.getMsisdnParam() == 0);
    BOOST_CHECK(object.getBalanceParam() == 0.0);
    BOOST_CHECK(object.getInputFilename() == std::string("no_file.xml"));
}

BOOST_AUTO_TEST_CASE(EmptyInputFilename)
{
    BOOST_CHECK_THROW(XmlToJsonConverter object(std::string("")), const char*);
}

BOOST_AUTO_TEST_CASE(XmlParserEmptyFile)
{
    boost::filesystem::ofstream output("special_existing_file.xml");
    XmlToJsonConverter object(std::string("special_existing_file.xml"));
    BOOST_CHECK(object.readXmlInputFile() == false);
    boost::filesystem::remove("special_existing_file.xml");
}

BOOST_AUTO_TEST_CASE(NonExistingFile)
{
    XmlToJsonConverter object(std::string("non_existing_file.xml"));
    BOOST_CHECK(object.readXmlInputFile() == false);
    BOOST_CHECK(object.getMsisdnParam() == 0);
    BOOST_CHECK(object.getBalanceParam() == 0.0);
}

BOOST_AUTO_TEST_CASE(JsonCreateWithoutXmlParser)
{
    XmlToJsonConverter object(std::string("default_json_file.xml"));
    BOOST_CHECK(object.writeJsonOutputFile() == true);
    BOOST_CHECK(boost::filesystem::exists("default_json_file.json") == true);
    boost::filesystem::remove("default_json_file.json");
}

BOOST_AUTO_TEST_SUITE_END()
